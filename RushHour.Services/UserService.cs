﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using RushHour.Domain;
using RushHour.Domain.Contracts.Repositories;
using RushHour.Domain.Contracts.Services;
using RushHour.Domain.DTOs;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Services
{
    public class UserService : IUserService
    {
        private readonly JwtSettings _jwtSettings;
        private readonly IUserRepository _userRepository;

        public UserService(IOptions<JwtSettings> options, IUserRepository userRepository)
        {
            _jwtSettings = options.Value;
            _userRepository = userRepository;
        }

        public async Task<string> GenerateJwtTokenAsync(UserDto userDto)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.Key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken
            (
                issuer: _jwtSettings.Issuer,
                audience: _jwtSettings.Audience,
                claims: await AddClaimsAsync(userDto),
                expires: DateTime.UtcNow.AddMinutes(_jwtSettings.ExpirationInMinutes),
                signingCredentials: credentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private async Task<List<Claim>> AddClaimsAsync(UserDto userDto)
        {
            UserDto user = await _userRepository.GetUserByEmailAsync(userDto.Email);

            List<Claim> claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, userDto.Email),
                new Claim(JwtRegisteredClaimNames.Iat, new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds().ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, user.Id.ToString())
            };

            var userRoles = await _userRepository.GetUserRolesAsync(userDto);

            if (userRoles.Contains("User"))
                claims.Add(new Claim(ClaimTypes.Role, "User"));
            else if (userRoles.Contains("Admin"))
                claims.Add(new Claim(ClaimTypes.Role, "Admin"));

            return claims;
        }

        public async Task<string> LoginUserAsync(string email, string password)
        {       
            var userDto = await _userRepository.GetUserByEmailAsync(email);

            if (userDto == null)
                throw new ArgumentNullException("User not found");

            if (!await _userRepository.CheckPasswordAsync(userDto, password))
                throw new ArgumentException("Invalid password");
           
            return await GenerateJwtTokenAsync(userDto);
        }

        public async Task<IdentityResult> RegisterUserAsync(string email, string firstName, string lastName, string password)
        {
            UserDto userDto = new UserDto()
            {
                Email = email,
                FirstName = firstName,
                LastName = lastName
            };

            var result = await _userRepository.RegisterUserAsync(userDto, password);

            return result;
        }

        public async Task<bool> CheckIfUserExistsAsync(Guid id) 
        {
           return await _userRepository.CheckIfUserExistsAsync(id);
        }
    }
}
