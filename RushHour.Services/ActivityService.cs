﻿using RushHour.Domain.Contracts.Repositories;
using RushHour.Domain.Contracts.Services;
using RushHour.Domain.DTOs;

namespace RushHour.Services
{
    public class ActivityService : BaseService<ActivityDto, IActivityRepository>, IActivityService
    {
        public ActivityService(IActivityRepository repository) : base(repository)
        {

        }
    }
}
