﻿using RushHour.Domain.Contracts.Repositories;
using RushHour.Domain.Contracts.Services;
using RushHour.Domain.DTOs;
using RushHour.Domain.DTOs.Appointment;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Services
{
    public class AppointmentService : BaseService<AppointmentDto, IAppointmentRepository>, IAppointmentService
    {
        private readonly IActivityService _activityService;

        public AppointmentService(IAppointmentRepository repository, IActivityService activityService) : base(repository)
        {
            _activityService = activityService;
        }

        public async Task<BasePaginationDto<AppointmentDto>> GetPaginatedByUserAsync(AppointmentPaginatorDto dto) 
        {
            return await _repository.GetPaginatedByUserAsync(dto);
        }

        public async Task Cancel(Guid id) 
        {
            await _repository.Cancel(id);
        }

        public async Task<int> CalculateAppointmentDurationAsync(List<Guid> activities)
        {
            int duration = 0;

            foreach (Guid id in activities)
            {
                ActivityDto activity = await _activityService.GetByIdAsync(id);

                duration += activity.Duration;
            }

            return duration;
        }
    }
}
