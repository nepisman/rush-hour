﻿using RushHour.Domain.Contracts.Repositories;
using RushHour.Domain.Contracts.Services;
using RushHour.Domain.DTOs;
using System;
using System.Threading.Tasks;

namespace RushHour.Services
{
    public abstract class BaseService<TDto, TRepository> : IBaseService<TDto>
        where TDto : BaseDto    
        where TRepository : IBaseRepository<TDto>
    {
        protected readonly TRepository _repository;

        protected BaseService(TRepository repository)
        {
            _repository = repository;
        }

        public virtual async Task<Guid> AddAsync(TDto dto)
        {           
            return await _repository.AddAsync(dto);
        }

        public async Task DeleteAsync(Guid id)
        {
           await _repository.DeleteAsync(id);          
        }

        public async Task<TDto> GetByIdAsync(Guid id)
        {
           return await _repository.GetByIdAsync(id);
        }

        public async Task<BasePaginationDto<TDto>> GetPaginatedAsync(int page, int numOfItems)
        {
           return await _repository.GetPaginatedAsync(page, numOfItems); 
        }

        public async Task UpdateAsync(TDto dto, Guid id)
        {
            await _repository.UpdateAsync(dto, id);       
        }
    }
}
