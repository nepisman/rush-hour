﻿using Microsoft.Extensions.DependencyInjection;
using RushHour.Data.Repositories;
using RushHour.Domain.Contracts.Repositories;

namespace RushHour.Extensions
{
    public static class RepositoryExtension
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services) 
        {
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IActivityRepository, ActivityRepository>();
            services.AddTransient<IAppointmentRepository, AppointmentRepository>();

            return services;
        }
    }
}
