﻿using Microsoft.Extensions.DependencyInjection;
using RushHour.Domain.Contracts.Services;
using RushHour.Services;

namespace RushHour.Extensions
{
    public static class ServiceExtension
    {
        public static IServiceCollection AddServices(this IServiceCollection services) 
        {
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IActivityService, ActivityService>();
            services.AddTransient<IAppointmentService, AppointmentService>();

            return services;
        }
    }
}
