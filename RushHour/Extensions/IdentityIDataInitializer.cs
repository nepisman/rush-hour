﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using RushHour.Data.Entities;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.Extensions
{
    public static class IdentityIDataInitializer
    {
        public static void UseSeedIdentityData(this IApplicationBuilder app,
                                               UserManager<User> userManager,
                                               RoleManager<IdentityRole<Guid>> roleManager,
                                               IConfiguration configuration)
        {
            SeedRoles(roleManager).Wait();
            SeedAdmin(userManager, configuration).Wait();
        }

        private async static Task SeedRoles(RoleManager<IdentityRole<Guid>> roleManager) 
        {
            var roleNames = new[] { "User", "Admin" };

            foreach (var roleName in roleNames)            
                await CreateRole(roleManager, roleName);
            
        }

        private async static Task CreateRole(RoleManager<IdentityRole<Guid>> roleManager, string roleName) 
        {
            if (!await roleManager.RoleExistsAsync(roleName)) 
            {
                IdentityRole<Guid> role = new IdentityRole<Guid>()
                {
                    Name = roleName
                };

                await roleManager.CreateAsync(role);
            }
        }

        private async static Task SeedAdmin(UserManager<User> userManager, IConfiguration configuration) 
        {
            var adminUsers = await userManager.GetUsersInRoleAsync("Admin");

            if (!adminUsers.Any()) 
            {
                User user = new User()
                {
                    UserName = configuration["Admin:Email"],
                    FirstName = configuration["Admin:FirstName"],
                    LastName = configuration["Admin:LastName"],
                    Email = configuration["Admin:Email"]
                };

                var result = await userManager.CreateAsync(user, configuration["Admin:Password"]);

                if (result.Succeeded)
                    await userManager.AddToRoleAsync(user, "Admin");
            }
        }
    }
}
