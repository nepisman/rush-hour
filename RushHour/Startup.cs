using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RushHour.Data;
using RushHour.Data.Entities;
using RushHour.Domain;
using RushHour.Extensions;
using System;

namespace RushHour
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<JwtSettings>(options => Configuration.GetSection("JwtSettings").Bind(options));
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("RushHourConnectionString")));
            services.AddIdentity<User, IdentityRole<Guid>>(options => options.User.RequireUniqueEmail = true)
                    .AddEntityFrameworkStores<ApplicationDbContext>();
            services.AddControllers();
            services.AddSwagger();

            services.AddAutoMapper(typeof(AutoMapperProfile));
            services.AddRepositories();
            services.AddServices();

            services.AddAuth(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, UserManager<User> userManager, RoleManager<IdentityRole<Guid>> roleManager)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "RushHour v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseSeedIdentityData(userManager, roleManager, Configuration);

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
