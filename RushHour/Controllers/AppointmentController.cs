﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RushHour.Domain.Contracts.Services;
using RushHour.Domain.DTOs;
using RushHour.Domain.DTOs.Appointment;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RushHour.Controllers
{

    [ApiController]
    [Route("api/appointments")]
    public class AppointmentController : ControllerBase
    {
        private readonly IAppointmentService _appointmentService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        
        public AppointmentController(IAppointmentService appointmentService, IUserService userService, IMapper mapper)
        {
            _appointmentService = appointmentService;
            _userService = userService;
            _mapper = mapper;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        [Route("~/api/admin/appointments")]
        [HttpPost]
        public async Task<IActionResult> AdminAddAsync([FromBody]CreateAppointmentByAdminDto dto) 
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (!await _userService.CheckIfUserExistsAsync(dto.UserId))
                return BadRequest("User with given Id not found");

            AppointmentDto appointmentDto = _mapper.Map<AppointmentDto>(dto);
            appointmentDto.EndDate = dto.StartDate.AddMinutes(await _appointmentService.CalculateAppointmentDurationAsync(dto.Activities));

            Guid result = await _appointmentService.AddAsync(appointmentDto);

            return result != Guid.Empty ? CreatedAtAction("GetByIdAsync", new { id = appointmentDto.Id }, dto) : StatusCode(500);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        [HttpPut("~/api/admin/appointments/{id}")]
        public async Task<IActionResult> AdminUpdateAsync([FromBody] CreateAppointmentByAdminDto dto, Guid id)
        {
            if (id == Guid.Empty || !ModelState.IsValid)
                return BadRequest();

            if (await _appointmentService.GetByIdAsync(id) == null)
                return NotFound();

            AppointmentDto appointmentDto = _mapper.Map<AppointmentDto>(dto);
            appointmentDto.EndDate = dto.StartDate.AddMinutes(await _appointmentService.CalculateAppointmentDurationAsync(dto.Activities));

            await _appointmentService.UpdateAsync(appointmentDto, id);

            return NoContent();
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        [HttpDelete("~/api/admin/appointments")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var userId = Guid.Parse(HttpContext.User.Claims.First(x => x.Type == JwtRegisteredClaimNames.Jti).Value);

            if (id == Guid.Empty)
                return BadRequest();

            if (await _appointmentService.GetByIdAsync(id) == null)
                return NotFound();

            await _appointmentService.DeleteAsync(id);
            return NoContent();
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "User")]
        [HttpPost]
        public async Task<IActionResult> UserAddAsync([FromBody] CreateAppointmentByUserDto dto) 
        {
            var userId = Guid.Parse(HttpContext.User.Claims.First(x => x.Type == JwtRegisteredClaimNames.Jti).Value);

            if (!ModelState.IsValid)
                return BadRequest();

            AppointmentDto appointmentDto = _mapper.Map<AppointmentDto>(dto);
            appointmentDto.EndDate = dto.StartDate.AddMinutes(await _appointmentService.CalculateAppointmentDurationAsync(dto.Activities));
            appointmentDto.UserId = userId;

            Guid result = await _appointmentService.AddAsync(appointmentDto);

            return result != Guid.Empty ? CreatedAtAction("GetByIdAsync", new { id = appointmentDto.Id }, dto) : StatusCode(500);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, User")]
        [ActionName("GetByIdAsync")]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetByIdAsync(Guid id) 
        {
            var userId = Guid.Parse(HttpContext.User.Claims.First(x => x.Type == JwtRegisteredClaimNames.Jti).Value);
            var userRole = HttpContext.User.Claims.First(x => x.Type == ClaimTypes.Role).Value;

            if (id == Guid.Empty)
                return BadRequest();

            AppointmentDto result = await _appointmentService.GetByIdAsync(id);

            if (result == null)
                return NotFound("This appointment does not exist");

            if (userRole == "User" && result.UserId != userId)
                return Forbid();
                    
            return Ok(result);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, User")]
        [HttpGet("page")]
        public async Task<IActionResult> GetPaginatedAsync([FromQuery]AppointmentPaginatorDto dto) 
        {
            dto.UserId = Guid.Parse(HttpContext.User.Claims.First(x => x.Type == JwtRegisteredClaimNames.Jti).Value);
            var userRole = HttpContext.User.Claims.First(x => x.Type == ClaimTypes.Role).Value;

            if (dto.PageNumber == 0 || dto.NumOfItems == 0)
                return BadRequest();

            BasePaginationDto<AppointmentDto> result = null;

            if (userRole == "Admin")
                result = await _appointmentService.GetPaginatedAsync(dto.PageNumber, dto.NumOfItems);
            else
                result = await _appointmentService.GetPaginatedByUserAsync(dto);
            
            return result.DtoCollection.Any() ? Ok(result) : NotFound();
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "User")]
        [HttpPut("{id}")]
        public async Task<IActionResult> UserUpdateAsync([FromBody] CreateAppointmentByUserDto dto, Guid id) 
        {
            var userId = Guid.Parse(HttpContext.User.Claims.First(x => x.Type == JwtRegisteredClaimNames.Jti).Value);

            if (id == Guid.Empty || !ModelState.IsValid)
                return BadRequest();

            var appointment = await _appointmentService.GetByIdAsync(id);

            if (appointment == null)
                return NotFound("Appointment does not exist");

            if (appointment.UserId != userId)
                return Forbid();

            AppointmentDto appointmentDto = _mapper.Map<AppointmentDto>(dto);
            appointmentDto.EndDate = dto.StartDate.AddMinutes(await _appointmentService.CalculateAppointmentDurationAsync(dto.Activities));
            appointmentDto.UserId = userId;

            await _appointmentService.UpdateAsync(appointmentDto, id);

            return NoContent();
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "User")]
        [HttpPut("cancel/{id}")]
        public async Task<IActionResult> Cancel(Guid id) 
        {
            var userId = Guid.Parse(HttpContext.User.Claims.First(x => x.Type == JwtRegisteredClaimNames.Jti).Value);

            if (id == Guid.Empty)
                return BadRequest();

            var appointment = await _appointmentService.GetByIdAsync(id);

            if (appointment == null)
                return NotFound("Appointment does not exist");

            if (appointment.UserId != userId && appointment != null)
                return NotFound();

            await _appointmentService.Cancel(id);

            return NoContent();
        }
    }
}
