﻿using Microsoft.AspNetCore.Mvc;
using RushHour.Domain.Contracts.Services;
using System.Threading.Tasks;

namespace RushHour.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("login")]
        public async Task<IActionResult> LoginAsync(string email, string password) 
        {
            var result = await _userService.LoginUserAsync(email, password);

            if (result != null)
                return Ok(result);
            else
                return BadRequest(result);
        }

        [HttpPost("register")]
        public async Task<IActionResult> RegisterAsync(string email, string firstName, string lastName, string password)
        {
            var result = await _userService.RegisterUserAsync(email, firstName, lastName, password);

            if (result.Succeeded)
                return Ok(result);
            else
                return BadRequest(result);
        }
    }
}
