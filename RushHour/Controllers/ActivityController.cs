﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RushHour.Domain.Contracts.Services;
using RushHour.Domain.DTOs;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
    public class ActivityController : ControllerBase
    {
        private readonly IActivityService _service;

        public ActivityController(IActivityService service)
        {
            _service = service;
        }

        [ActionName("AddAsync")]
        [HttpPost]
        public async Task<IActionResult> AddAsync([FromBody]ActivityDto dto) 
        {
            if (!ModelState.IsValid)
                return BadRequest();

            Guid result = await _service.AddAsync(dto);
              
            return result != Guid.Empty ? CreatedAtAction("GetByIdAsync", new { id = dto.Id }, dto) : StatusCode(500);
        }

        [ActionName("GetByIdAsync")]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetByIdAsync(Guid id) 
        {
            if (id == Guid.Empty)
                return BadRequest();

            ActivityDto result = await _service.GetByIdAsync(id);

            return result != null ? Ok(result) : NotFound();
        }

        [ActionName("GetPaginatedAsync")]
        [HttpGet]
        public async Task<IActionResult> GetPaginatedAsync(int page, int numOfItems) 
        {
            if (page == 0 || numOfItems == 0)
                return BadRequest();

           BasePaginationDto<ActivityDto> result = await _service.GetPaginatedAsync(page, numOfItems);

            return result.DtoCollection.Any() ? Ok(result) : NotFound(result);
        }

        [ActionName("DeleteAsync")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid id) 
        {
            if (id == Guid.Empty)
                return BadRequest();

            if (await _service.GetByIdAsync(id) == null)
                return NotFound();

            await _service.DeleteAsync(id);

            return NoContent();
        }

        [ActionName("UpdateAsync")]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync([FromBody]ActivityDto dto, Guid id) 
        {
            if (id == Guid.Empty || !ModelState.IsValid)
                return BadRequest();

            if (await _service.GetByIdAsync(id) == null)
                return NotFound();

            await _service.UpdateAsync(dto, id);

            return NoContent();
        }
    }
}
