﻿using Microsoft.AspNetCore.Identity;
using RushHour.Domain.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Domain.Contracts.Repositories
{
    public interface IUserRepository
    {
        Task<IdentityResult> RegisterUserAsync(UserDto userDto, string password);

        Task<UserDto> GetUserByEmailAsync(string email);

        Task<bool> CheckPasswordAsync(UserDto userDto, string password);

        Task<IList<string>> GetUserRolesAsync(UserDto userDto);

        Task<bool> CheckIfUserExistsAsync(Guid id);
    }
}
