﻿using RushHour.Domain.DTOs;
using RushHour.Domain.DTOs.Appointment;
using System;
using System.Threading.Tasks;

namespace RushHour.Domain.Contracts.Repositories
{
    public interface IAppointmentRepository : IBaseRepository<AppointmentDto>
    {
        Task<BasePaginationDto<AppointmentDto>> GetPaginatedByUserAsync(AppointmentPaginatorDto dto);

        Task Cancel(Guid id);
    }
}
