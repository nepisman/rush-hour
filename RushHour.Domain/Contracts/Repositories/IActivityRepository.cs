﻿using RushHour.Domain.DTOs;

namespace RushHour.Domain.Contracts.Repositories
{
    public interface IActivityRepository : IBaseRepository<ActivityDto>
    {
    }
}
