﻿using RushHour.Domain.DTOs;

namespace RushHour.Domain.Contracts.Services
{
    public interface IActivityService : IBaseService<ActivityDto>
    {
    }
}
