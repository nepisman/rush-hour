﻿using RushHour.Domain.DTOs;
using RushHour.Domain.DTOs.Appointment;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Domain.Contracts.Services
{
    public interface IAppointmentService : IBaseService<AppointmentDto> 
    {
        Task<int> CalculateAppointmentDurationAsync(List<Guid> activities);

        Task<BasePaginationDto<AppointmentDto>> GetPaginatedByUserAsync(AppointmentPaginatorDto dto);

        Task Cancel(Guid id);
    }

}
