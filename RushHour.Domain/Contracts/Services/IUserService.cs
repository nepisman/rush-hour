﻿using Microsoft.AspNetCore.Identity;
using RushHour.Domain.DTOs;
using System;
using System.Threading.Tasks;

namespace RushHour.Domain.Contracts.Services
{
    public interface IUserService
    {
        Task<IdentityResult> RegisterUserAsync(string email, string firstName, string lastName, string password);

        Task<string> LoginUserAsync(string email, string password);

        Task<string> GenerateJwtTokenAsync(UserDto userDto);

        Task<bool> CheckIfUserExistsAsync(Guid id);
    }
}
