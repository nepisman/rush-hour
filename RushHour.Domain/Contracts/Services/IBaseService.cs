﻿using RushHour.Domain.DTOs;
using System;
using System.Threading.Tasks;

namespace RushHour.Domain.Contracts.Services
{
    public interface IBaseService<TDto>
        where TDto : BaseDto   
    {
        Task<Guid> AddAsync(TDto dto);

        Task<TDto> GetByIdAsync(Guid id);

        Task<BasePaginationDto<TDto>> GetPaginatedAsync(int page, int numOfItems);

        Task DeleteAsync(Guid id);

        Task UpdateAsync(TDto dto, Guid id);
    }
}
