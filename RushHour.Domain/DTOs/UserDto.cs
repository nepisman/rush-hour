﻿namespace RushHour.Domain.DTOs
{
    public class UserDto : BaseDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }  

        public string PasswordHash { get; set; }
    }
}
