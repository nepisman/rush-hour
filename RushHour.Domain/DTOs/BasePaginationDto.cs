﻿using System.Collections.Generic;

namespace RushHour.Domain.DTOs
{
    public class BasePaginationDto<TDto>
        where TDto : BaseDto
    {
        public IEnumerable<TDto> DtoCollection { get; set; }

        public int PageNumber { get; set; }

        public int NumOfItems { get; set; }
    }
}
