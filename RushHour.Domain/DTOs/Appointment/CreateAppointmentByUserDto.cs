﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RushHour.Domain.DTOs.Appointment
{
    public class CreateAppointmentByUserDto 
    {
        [Required(ErrorMessage = "Start date is required")]
        public DateTime StartDate { get; set; }

        public List<Guid> Activities { get; set; }
    }
}
