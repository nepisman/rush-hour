﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RushHour.Domain.DTOs.Appointment
{
    public class CreateAppointmentByAdminDto
    {
        [Required(ErrorMessage = "Start date is required")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = "UserId is required")]
        public Guid UserId { get; set; }

        public List<Guid> Activities { get; set; }
    }
}
