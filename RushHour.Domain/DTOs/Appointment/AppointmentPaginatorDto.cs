﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;

namespace RushHour.Domain.DTOs.Appointment
{
    public class AppointmentPaginatorDto : BasePaginatorDto
    {
        [BindNever]
        public Guid UserId { get; set; }
    }
}
