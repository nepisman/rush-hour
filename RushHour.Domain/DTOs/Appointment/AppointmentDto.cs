﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RushHour.Domain.DTOs.Appointment
{
    public class AppointmentDto : BaseDto
    {
        [Required(ErrorMessage = "Start date is required")]
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public Guid UserId { get; set; }

        public List<ActivityDto> Activities { get; set; }
    }
}
