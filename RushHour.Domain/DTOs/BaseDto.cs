﻿using System;

namespace RushHour.Domain.DTOs
{
    public class BaseDto
    {
        public Guid Id { get; set; }
    }
}
