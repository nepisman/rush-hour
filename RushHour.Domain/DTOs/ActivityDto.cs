﻿using System.ComponentModel.DataAnnotations;

namespace RushHour.Domain.DTOs
{
    public class ActivityDto : BaseDto
    {
        [Required(ErrorMessage = "Name is required")]
        [RegularExpression(@"^([a-zA-Z]+|[a-zA-Z]+\s[a-zA-Z]+)*$", ErrorMessage = "Only capital and small letters allowed")]
        [StringLength(100, ErrorMessage = "Name can be no longer than 100 characters")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Duration is required")]
        [RegularExpression(@"([1-9][0-9]*)", ErrorMessage = "Only integer number allowed")]
        public int Duration { get; set; }

        [Required(ErrorMessage = "Price is required")]
        [RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Only 2 decimal points allowed ")]
        public decimal Price { get; set; }
    }
}
