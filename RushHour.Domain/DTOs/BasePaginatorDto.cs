﻿namespace RushHour.Domain.DTOs
{
    public class BasePaginatorDto
    {
        public int PageNumber { get; set; }

        public int NumOfItems { get; set; }
    }
}
