﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RushHour.Data.Entities;
using RushHour.Domain.Contracts.Repositories;
using RushHour.Domain.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.Data.Repositories
{
    public abstract class BaseRepository<TDto, TEntity> : IBaseRepository<TDto>
        where TDto : BaseDto
        where TEntity : BaseEntity       
    {
        protected readonly ApplicationDbContext _context;
        protected readonly IMapper _mapper;

        protected BaseRepository(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
     
        public async Task<Guid> AddAsync(TDto dto)
        {
            TEntity entity = _mapper.Map<TEntity>(dto);

            OnBeforeInsert(entity);

            await _context.AddAsync(entity);
            await _context.SaveChangesAsync();

            Guid id = entity.Id;

            return id;
        }

        public async Task DeleteAsync(Guid id)
        {
             TEntity entity = await _context.Set<TEntity>()
                                            .FirstOrDefaultAsync(e => e.Id == id);

            _context.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<TDto> GetByIdAsync(Guid id)
        {
            var entities = _context.Set<TEntity>().AsNoTracking();
            entities = OnBeforeGet(entities);

            var result = await entities.FirstOrDefaultAsync(e => e.Id == id);

            return _mapper.Map<TDto>(result);
        }

        public async Task<BasePaginationDto<TDto>> GetPaginatedAsync(int page, int numOfItems)
        {
            var entities = _context.Set<TEntity>().AsQueryable();
                                        
            entities = OnBeforeGet(entities);

            var result = await entities.Skip((page - 1) * numOfItems)
                                       .Take(numOfItems)
                                       .ToListAsync();

            BasePaginationDto<TDto> paginationDto = new BasePaginationDto<TDto>
            {
                PageNumber = page,
                NumOfItems = numOfItems,
                DtoCollection = _mapper.Map<IEnumerable<TDto>>(result)
            };

            return paginationDto;
        }

        public virtual async Task UpdateAsync(TDto dto, Guid id)
        {
            TEntity entity = await _context.Set<TEntity>()
                                           .FirstOrDefaultAsync(entity => entity.Id == id);

            _mapper.Map(dto, entity);
            await _context.SaveChangesAsync();
        }

        protected virtual  void OnBeforeInsert(TEntity entityToCreate)
        {

        }

        protected virtual IQueryable<TEntity> OnBeforeGet(IQueryable<TEntity> entities)
        {
            return entities;
        }
    }
}
