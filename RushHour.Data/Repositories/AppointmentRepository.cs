﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RushHour.Data.Entities;
using RushHour.Domain.Contracts.Repositories;
using RushHour.Domain.DTOs;
using RushHour.Domain.DTOs.Appointment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.Data.Repositories
{
    public class AppointmentRepository : BaseRepository<AppointmentDto, Appointment>, IAppointmentRepository
    {
        public AppointmentRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {

        }

        public async Task<BasePaginationDto<AppointmentDto>> GetPaginatedByUserAsync(AppointmentPaginatorDto dto)
        {
            var appointments =  _context.Appointments.AsQueryable();

            appointments = OnBeforeGet(appointments);
                                             
            var result = await appointments.Where(a => a.UserId == dto.UserId)
                                           .Skip((dto.PageNumber - 1) * dto.NumOfItems)
                                           .Take(dto.NumOfItems)
                                           .ToListAsync();

            BasePaginationDto<AppointmentDto> paginationDto = new BasePaginationDto<AppointmentDto>
            {
                PageNumber = dto.PageNumber,
                NumOfItems = dto.NumOfItems,
                DtoCollection = _mapper.Map<IEnumerable<AppointmentDto>>(result)
            };

            return paginationDto;
        }

        public override async Task UpdateAsync(AppointmentDto dto, Guid id)
        {
            var appointments =  _context.Appointments.AsQueryable();
                                                    
            appointments = OnBeforeGet(appointments);

            var result = await appointments.FirstOrDefaultAsync(a => a.Id == id);

            result.Activities.Clear();
            await _context.SaveChangesAsync();

            _mapper.Map(dto, result);
            OnBeforeInsert(result);

            await _context.SaveChangesAsync();
        }

        public async Task Cancel(Guid id) 
        {
            Appointment appointment = await _context.Appointments
                                                    .FirstOrDefaultAsync(a => a.Id == id);

            appointment.IsCancelled = true;

            await _context.SaveChangesAsync();
        }

        protected override void OnBeforeInsert(Appointment appointment) 
        {
            foreach (var activity in appointment.Activities)
            {
                _context.Activities.Attach(activity);
            }
        }

        protected override IQueryable<Appointment> OnBeforeGet(IQueryable<Appointment> entities)
        {
            return base.OnBeforeGet(entities).Include(a => a.Activities);
        }
    }
}
