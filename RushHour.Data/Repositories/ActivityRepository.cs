﻿using AutoMapper;
using RushHour.Data.Entities;
using RushHour.Domain.Contracts.Repositories;
using RushHour.Domain.DTOs;

namespace RushHour.Data.Repositories
{
    public class ActivityRepository : BaseRepository<ActivityDto, Activity>, IActivityRepository
    {
        public ActivityRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {

        }
    }
}
