﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using RushHour.Data.Entities;
using RushHour.Domain.Contracts.Repositories;
using RushHour.Domain.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;

        public UserRepository(UserManager<User> userManager, IMapper mapper)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<bool> CheckPasswordAsync(UserDto userDto, string password)
        {
            User user = _mapper.Map<User>(userDto);

            return await _userManager.CheckPasswordAsync(user, password);
        }

        public async Task<UserDto> GetUserByEmailAsync(string email)
        {
            User user =  await _userManager.FindByEmailAsync(email);

            return _mapper.Map<UserDto>(user);
        }

        public async Task<IList<string>> GetUserRolesAsync(UserDto userDto)
        {
            User user = _mapper.Map<User>(userDto);

            return await _userManager.GetRolesAsync(user);
        }

        public async Task<IdentityResult> RegisterUserAsync(UserDto userDto, string password)
        {
            User user = _mapper.Map<User>(userDto);

            IdentityResult result = await _userManager.CreateAsync(user, password);

            if (result.Succeeded)
                await _userManager.AddToRoleAsync(user, "User");

            return result;
        }

        public async Task<bool> CheckIfUserExistsAsync(Guid id) 
        {
            return await _userManager.FindByIdAsync(id.ToString()) != null;
        }   
    }
}
