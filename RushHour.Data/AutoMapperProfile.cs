﻿using AutoMapper;
using RushHour.Data.Entities;
using RushHour.Domain.DTOs;
using RushHour.Domain.DTOs.Appointment;
using System;

namespace RushHour.Data
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>()
                    .ForMember(d => d.UserName, options => options.MapFrom(src => src.Email));
                                 
            CreateMap<Activity, ActivityDto>().ReverseMap();

            CreateMap<Appointment, AppointmentDto>();

            CreateMap<AppointmentDto, Appointment>()
                    .ForMember(a => a.Id, options => options.Ignore())
                    .ForMember(a => a.IsCancelled, options => options.Ignore());
            CreateMap<CreateAppointmentByAdminDto, AppointmentDto>()
                    .ForMember(a => a.Id, options => options.Ignore())
                    .ForMember(d => d.EndDate, options => options.MapFrom(src => src.StartDate));

            CreateMap<CreateAppointmentByUserDto, AppointmentDto>()
                    .ForMember(a => a.Id, options => options.Ignore())
                    .ForMember(d => d.EndDate, options => options.MapFrom(src => src.StartDate))
                    .ForMember(d => d.UserId, options => options.Ignore());
            CreateMap<Guid, ActivityDto>().ConvertUsing(id => new ActivityDto { Id = id, Name = string.Empty });                    
        }     
    }
}
