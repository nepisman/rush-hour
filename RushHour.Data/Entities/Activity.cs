﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RushHour.Data.Entities
{
    public class Activity : BaseEntity
    {
        [Column(TypeName = "nvarchar(100)")]
        [Required]
        public string Name { get; set; }
       
        public int Duration { get; set; }

        [Column(TypeName = "decimal(8,2)")]
        public decimal Price { get; set; }

        public ICollection<Appointment> Appointments { get; set; }
    }
}
