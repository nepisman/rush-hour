﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace RushHour.Data.Entities
{
    public class Appointment : BaseEntity
    {
        [Column(TypeName = "smalldatetime")]
        public DateTime StartDate { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime EndDate { get; set; }

        public Guid UserId { get; set; }
        
        public User User { get; set; }

        public bool IsCancelled { get; set; }

        public ICollection<Activity> Activities { get; set; }
    }
}
